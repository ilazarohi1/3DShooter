# README - Juego de disparos

https://youtu.be/7IzhMIC2t2I

## Descripción del Proyecto

Este juego te sumerge en un mundo lleno de desafíos y peligros. Explora diversos escenarios, desde terrenos montañosos hasta instalaciones urbanas, enfrentándote a enemigos y resolviendo intrincados puzles para avanzar.

### Características Principales

- **Escenarios Diversificados:** Explora terrenos montañosos, áreas urbanas y entornos cerrados llenos de desafíos y secretos.
- **Armas Variadas:** Utiliza dos tipos de armas diferentes: una pistola precisa de cadencia lenta y una ametralladora de largo alcance y fuego rápido.
- **Sistema de Escudo:** Protege tu vida con un escudo que absorbe parte del daño recibido. Administra sabiamente tu escudo y tu salud para sobrevivir.
- **HUD Intuitivo:** Mantén un ojo en tu vida, escudo, tipo de arma y munición en todo momento gracias a la interfaz de usuario intuitiva.
- **Controles Intuitivos:** Controla tu personaje con los botones AWSD, salta con SPACE, dispara con click izquierdo y cambia de arma con la rueda del ratón.

### Experiencia de Juego

- **Sigilo Emphasizado:** Aunque el juego presenta mecánicas de acción, se enfatiza el sigilo como una estrategia viable. Los jugadores pueden evitar enfrentamientos directos utilizando tácticas de sigilo para pasar desapercibidos ante los enemigos.
- **Escasez de Munición y Objetos de Recuperación:** Los recursos son limitados en el juego. La munición es escasa y los objetos de recuperación, como salud y escudo, son difíciles de encontrar. Los jugadores deben administrar cuidadosamente sus recursos y explorar el entorno para encontrar objetos que les ayuden a sobrevivir.
- **Enfrentamientos Estratégicos:** Los jugadores deben planificar sus movimientos y enfrentamientos con cautela. Utiliza el entorno a tu favor y elige el momento adecuado para atacar o esquivar a los enemigos.

### Tecnología Utilizada

- **Unity Versión:** 2022.3.20f1

¡Prepárate para la acción y sumérgete en este desafiante juego!
